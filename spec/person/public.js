// A public profile of a person discovery by anyone in their network
const { string, image, gender } = require('../lib/field-types')
const { isPublic } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const publicPersonSpec = {
  type: 'profile/person',
  props: {
    preferredName: string,
    avatarImage: image,
    gender
  },
  hooks: {
    isRoot: [isPublic],
    isUpdate: [isPublic]
  }
}
publicPersonSpec.getTransformation = GetTransformation(publicPersonSpec)

module.exports = publicPersonSpec
