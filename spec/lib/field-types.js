const ComplexSet = require('@tangle/complex-set')
const Overwrite = require('@tangle/overwrite')
const OverwriteFields = require('@tangle/overwrite-fields')

const { blobSchema, hyperBlobSchema } = require('./blob-schemas')
const image = require('ssb-schema-definitions/definition/image')
const feedIdPattern = require('ssb-schema-definitions/definition/primitives').feedId.pattern

const overwrite = (schema, opts = {}) => Overwrite({ valueSchema: schema, ...opts })
const overwriteFields = (schema, opts = {}) => OverwriteFields({ valueSchema: schema, ...opts })

const stringArray = {
  type: ['array', 'null'],
  uniqueItems: true,
  items: { type: 'string' }
}

const visibleBy = {
  enum: ['members', 'admin']
}

const dateCustomField = { // NOTE: we had to do date differently here becayse the first case would pass any string,
  // meaning all date values would pass validation as it would always be a string
  type: 'object',
  required: ['type', 'value'],
  properties: {
    type: { type: 'string', pattern: '^date$' },
    value: { type: 'string' } // validated using validators
  },
  additionalProperties: false
}

// allows us to do this
// const customFields = {
//   [customFieldKey]: Blob
// }
const blobField = {
  required: true,
  oneOf: [blobSchema, hyperBlobSchema]
}

const blobArray = {
  type: ['array'],
  // uniqueItems: true, // we may not want to do this as it will fail if say someone adds two photos that are the same
  items: blobField
}

module.exports = {
  authors: ComplexSet(`^(\\*|${feedIdPattern})$`),
  string: overwrite({ type: ['string', 'null'] }),
  boolean: overwrite({ type: ['boolean', 'null'] }),
  number: overwrite({
    type: ['number', 'null'],
    multipleOf: 1,
    minimum: 1 // > 0
  }),
  tombstone: overwrite({ $ref: '#/definitions/tombstone' }),
  gender: overwrite({
    enum: ['female', 'male', 'other', 'unknown', null]
  }),
  profileSource: overwrite({
    enum: ['ahau', 'webForm', null]
  }),
  image: overwrite({
    ...image,
    type: ['object', 'null']
  }),
  customForms: overwrite(
    {
      type: ['array', 'null'],
      uniqueItems: true,
      items: {
        type: 'object',
        required: ['type', 'label'],
        properties: {
          type: {
            enum: ['input', 'textarea']
          }, // type of form input field
          label: { type: 'string' } // the label for the form input field
        }
      }
    },
    { reifiedIdentity: [] }
  ),
  stringArray: overwrite(
    stringArray,
    { reifiedIdentity: [] }
  ),
  poBoxId: overwrite({
    type: ['string', 'null'],
    pattern: '^ssb:identity/po-box/' // TODO check the tail more closely
  }),
  // defines the different types of custom fields on a community public profile
  customFieldDefs: overwriteFields(
    {
      type: 'object',
      oneOf: [
        {
          type: 'object',
          required: ['type', 'label', 'required', 'visibleBy'],
          properties: {
            type: { enum: ['text', 'array', 'checkbox', 'number', 'date'] },
            label: { type: 'string' },
            required: { type: 'boolean' },
            visibleBy,
            order: { type: 'number' },
            tombstone: { $ref: '#/definitions/tombstone' }
          },
          additionalProperties: false
        },
        { // special type "list" has multiple and options fields
          type: 'object',
          required: ['type', 'label', 'required', 'options', 'visibleBy', 'multiple'],
          properties: {
            type: {
              type: 'string',
              pattern: '^list$'
            },
            label: { type: 'string' },
            required: { type: 'boolean' },
            multiple: { type: 'boolean' },
            visibleBy,
            order: { type: 'number' },
            options: stringArray,
            tombstone: { $ref: '#/definitions/tombstone' }
          },
          additionalProperties: false
        },
        { // special type "file" has multiple and description
          type: 'object',
          required: ['type', 'label', 'fileTypes', 'description', 'required', 'visibleBy', 'multiple'],
          properties: {
            type: {
              type: 'string',
              pattern: '^file$'
            },
            label: { type: 'string' },
            description: { type: 'string' },
            fileTypes: {
              type: 'array',
              minItems: 1,
              maxItems: 4,
              items: {
                type: 'string',
                enum: ['photo', 'video', 'audio', 'document']
              }
            },
            required: { type: 'boolean' },
            multiple: { type: 'boolean' },
            visibleBy,
            order: { type: 'number' },
            tombstone: { $ref: '#/definitions/tombstone' }
          },
          additionalProperties: false
        }
      ]
    },
    { keyPattern: '^[0-9]+$' } // opts NOTE: this keyPattern is designed to only work with timestamps
  ),
  // defines the custom field allowed on a person profile
  customFields: overwriteFields(
    {
      nullable: true,
      oneOf: [
        { type: 'string' }, // for text
        { type: 'boolean' }, // for checkbox
        { type: 'number' }, // for numbers including ints and floats
        stringArray, // for array and list
        dateCustomField,

        // allow files as the value
        blobField, // allow a single blob
        blobArray // allow an array of blobs
      ]
    },
    { keyPattern: '^[0-9]+$' } // opts
  )
}
