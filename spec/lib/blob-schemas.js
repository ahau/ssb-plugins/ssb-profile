/**
 * This code was take from ssb-artefacts/spec/lib/field-schemas.js
 */

const base64Pattern = '^[a-zA-Z0-9\\/\\+]{42}[AEIMQUYcgkosw048]=$' // 32 bytes
// copied from is-canonical-base64
const uuidPattern = '^(?:[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|00000000-0000-0000-0000-000000000000)$'
// copied from uuid/src/regex (note this version is case sensitive)

const blobSchema = {
  type: 'object',
  required: ['blobId', 'mimeType'],
  properties: {
    type: { type: 'string', pattern: '^ssb$' }, // new so NOT required
    blobId: { $ref: '#/definitions/blobId' },
    unbox: {
      type: 'string',
      minLength: 49,
      maxLength: 49,
      pattern: base64Pattern.replace('$', '\\.boxs$')
    },
    mimeType: {
      type: 'string',
      pattern: '^(image|audio|video|application|text)/.+$'
    },
    size: { type: 'integer' }
  },
  additionalProperies: false
}

const hyperBlobSchema = {
  type: 'object',
  required: ['type', 'driveAddress', 'blobId', 'readKey', 'mimeType'],
  properties: {
    type: { type: 'string', pattern: '^hyper$' },
    driveAddress: { type: 'string', pattern: base64Pattern },
    blobId: { type: 'string', pattern: uuidPattern },
    readKey: { type: 'string', pattern: base64Pattern },
    mimeType: {
      type: 'string',
      pattern: '^(image|audio|video|application|text)/.+$'
    },
    size: { type: 'integer' }
  },
  additionalProperies: false
}

module.exports = {
  blobSchema,
  hyperBlobSchema
}
