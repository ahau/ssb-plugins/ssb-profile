const { string, boolean, image, customForms, poBoxId, customFieldDefs } = require('../lib/field-types')
const { isPublic } = require('../lib/validators')
const GetTransformation = require('../lib/get-transformation')

const publicCommunitySpec = {
  type: 'profile/community',
  props: {
    preferredName: string,
    description: string,

    address: string,
    city: string,
    country: string,
    postCode: string,
    phone: string,
    email: string,

    poBoxId,

    avatarImage: image,
    headerImage: image,

    // these fields are only on public community profiles
    joiningQuestions: customForms,
    customFields: customFieldDefs,

    // public settings
    acceptsVerifiedCredentials: boolean,
    issuesVerifiedCredentials: boolean

  },
  hooks: {
    isRoot: [isPublic],
    isUpdate: [isPublic]
  }
}
publicCommunitySpec.getTransformation = GetTransformation(publicCommunitySpec)

module.exports = publicCommunitySpec
