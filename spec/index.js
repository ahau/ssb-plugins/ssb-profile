module.exports = {
  person: {
    source: require('./person/source'),
    group: require('./person/group'),
    admin: require('./person/admin'),
    public: require('./person/public')
  },
  community: {
    group: require('./community/group'),
    public: require('./community/public')
  },
  pataka: {
    public: require('./pataka/public')
  },

  link: {
    feed: require('./link/feed-profile'),
    group: require('./link/group-profile'),
    admin: require('./link/profile-profile-admin')
  }
}
