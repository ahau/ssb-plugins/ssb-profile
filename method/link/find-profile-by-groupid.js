const { isCloakedMsg: isGroup } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const pileSort = require('pile-sort')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function FindByGroupId (ssb, crut, getProfile) {
  const getActiveProfile = (profileId, cb) => {
    getProfile(profileId, (err, profile) => {
      if (err) return cb(null, null)
      if (profile.tombstone) return cb(null, null)
      if (profile.states.length && profile.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" profile means no profile / tombstoned

      cb(null, profile)
    })
  }

  return function findByGroupId (groupId, opts = {}, cb) {
    if (typeof opts === 'function') return findByGroupId(groupId, {}, opts)
    if (!isGroup(groupId)) return cb(new Error('requires a valid groupId'))

    pull(
      // TODO replace with crut.list
      ssb.db.query(
        where(type('link/group-profile')),
        toPullStream()
      ),
      pull.filter(crut.isRoot),
      pull.filter(m => m.value.content.parent === groupId),
      pull.map(link => link.value.content.child),
      paraMap(opts.getProfile || getActiveProfile, 4),
      pull.filter(Boolean), // drop null profiles
      pull.map(profile => {
        profile.type = profile.type.replace('profile/', '')
        return profile
      }),
      pull.collect((err, results) => {
        if (err) return cb(err)

        const piles = pileSort(results, [
          res => !res.recps || res.recps.length === 0 // public
          // remainder are private
        ])

        cb(null, { public: piles[0], private: piles[1] })
      })
    )
  }
}
