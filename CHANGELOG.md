# Changelog

## 5.20.0

- Add the following field to a public community profile
  - `issuesVerifiedCredentials`

## 5.19.0

- Add the following field to a public community profile
  - `acceptsVerifiedCredentials`

## TODO: 5.8.^ - 5.18.0

## 5.8.0

- Add the following fields to a community profile
  - allowWhakapapaViews
  - allowPersonsList
  - allowStories

## 5.7.0

- Update versions of `ssb-crut` and `ssb-crut-authors`

## 5.6.0

- Allow ssb.profile.find to work for special characters including macrons
  - You can now search for a person whos name has a macron, without using a macron. E.g. searching for maui returns profiles for māui AND maui

## 5.0.0

### Breaking changes

- privatePerson has had all contact fields removed (email/ address/ phone)
    - existing records with these fields will still work, those fields just won't be present on `get`
    - new records should not save these fields

### New Features

- new profile types:
  - `profiles/person/source`
  - `profiles/person/admin`


### Deprecated APIs
- `ssb.profile.privatePerson` => `ssb.profile.person.group`
- `ssb.profile.publicPerson` => `ssb.profile.person.public`
- `ssb.profile.privateCommunity` => `ssb.profile.community.group`
- `ssb.profile.publicCommunity` => `ssb.profile.community.public`
- `ssb.profile.publicPataka` => `ssb.profile.pataka.public`


