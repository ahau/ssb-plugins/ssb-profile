const test = require('tape')

const Server = require('./test-bot')
const legacyCrut = require('../legacy')

test('ssb-profile/legacy', t => {
  const ssb = Server()

  const crut = legacyCrut(ssb)

  const details = {
    authors: { add: [ssb.id] }
  }

  crut.create('person', details, (err) => {
    console.log('↑ expected console.trace')
    t.error(err, 'create person.public')

    crut.create('community', details, (err) => {
      t.error(err, 'create community.public')

      t.pass('only 1 warning seen')

      ssb.close()
      t.end()
    })
  })
})
