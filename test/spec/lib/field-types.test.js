const test = require('tape')
const { gender, customForms, image, profileSource: source } = require('../../../spec/lib/field-types')
const printVal = value => typeof value === 'string'
  ? `'${value}'`
  : JSON.stringify(value)

test('field-types (gender)', t => {
  const isValidGender = (value, bool = true) => {
    const result = gender.isValid({ set: value })

    t[bool](Boolean(result), `${typeof value === 'string' ? `'${value}'` : value} = ${bool}`)

    if (Boolean(result) !== bool && gender.isValid.errors) {
      console.log(gender.isValid.errors)
    }
  }

  isValidGender(null, true)
  isValidGender('male', true)
  isValidGender('female', true)
  isValidGender('other', true)
  isValidGender('unknown', true)

  isValidGender('androgene', false)
  isValidGender(4, false)
  isValidGender(['male', 'female'], false)

  t.end()
})

test('field-types (customForms)', t => {
  const isValid = (value, bool = true) => {
    const result = customForms.isValid({ set: value })

    t[bool](Boolean(result), `${printVal(value)} = ${bool}`)

    if (Boolean(result) !== bool && customForms.isValid.errors) {
      console.log(customForms.isValid.errors)
    }
  }

  isValid(null)
  isValid([{ type: 'input', label: 'location' }])
  isValid([{ type: 'textarea', label: 'location' }])

  isValid([
    { type: 'input', label: 'location' },
    { type: 'textarea', label: 'story' }
  ])

  isValid([{ type: 'input' }], false)
  isValid([{ label: 'things' }], false)
  isValid([
    { type: 'input', label: 'location' },
    { type: 'input', label: 'location' }
  ], false)

  t.end()
})

test('field-types (image)', t => {
  const isValid = (value, bool = true) => {
    const result = image.isValid({ set: value })

    t[bool](Boolean(result), `${printVal(value)} = ${bool}`)

    if (Boolean(result) !== bool && image.isValid.errors) {
      console.log(image.isValid.errors)
    }
  }

  const headerImage = {
    blob: '&U8R5rtLuC4jyNNm+qKp2sDs+b/rE34ITdrkG3gkNTYQ=.sha256',
    unbox: 'vk/5VC+861LuGxslhhztchb4Qynse2VvzNdKUD4vD5k=.boxs',
    mimeType: 'image/png',
    size: 294695
  }

  isValid(null)
  isValid(headerImage)

  isValid({ size: 294695 }, false) // missing required fields
  isValid({}, false) // dont allow no fields

  isValid(false, false)
  isValid(123, false)
  isValid('dog', false)

  t.end()
})

test('field-types (source)', t => {
  const isValidSource = (value, bool = true) => {
    const result = source.isValid({ set: value })

    t[bool](Boolean(result), `${typeof value === 'string' ? `'${value}'` : value} = ${bool}`)

    if (Boolean(result) !== bool && source.isValid.errors) {
      console.log(source.isValid.errors)
    }
  }

  isValidSource(null, true)
  isValidSource('ahau', true)
  isValidSource('webForm', true)

  isValidSource('androgene', false)
  isValidSource(4, false)
  isValidSource(['male', 'female'], false)

  t.end()
})
