const test = require('tape')
const CRUT = require('ssb-crut-authors')
const publicCommunitySpec = require('../../../spec').community.public
const Server = require('../../test-bot')

test('profile/community/public customFields', t => {
  t.plan(38)
  const mock = (value) => {
    return {
      type: 'profile/community',
      customFields: value,
      tangles: {
        profile: { root: null, previous: null }
      }
    }
  }

  let i = 0
  const generateTimestamp = () => {
    return (Date.now() + (i += 10)).toString()
  }

  const server = Server()
  const { isRoot } = new CRUT(server, publicCommunitySpec)

  const mockTest = (value, isError = false) => {
    const timestamp = generateTimestamp()
    const field = {
      [timestamp]: value
    }

    if (isError) {
      t.false(isRoot(mock(field)), `isRoot(${value}) => false`)
      t.notEquals(isRoot.errors, null, `customFields: ${value} => returns errors`)
      return
    }

    t.true(isRoot(mock(field)), `isRoot ${value.type}`)
    t.equals(isRoot.errors, null, `customFields: ${value.type} => no errors`)
  }

  const mockFailingTest = (value) => mockTest(value, true)

  // text: a string as the value
  mockTest({
    type: 'text',
    label: 'Hometown',
    order: 1,
    required: false,
    visibleBy: 'members'
  })

  // multiple response: the value is an array of strings
  mockTest({
    type: 'array',
    label: 'Qualifications',
    order: 2.2,
    required: false,
    visibleBy: 'members'
  })

  // select: choose from a list of options
  mockTest({
    type: 'list',
    label: 'Employement Status',
    required: false,
    order: 3.3,
    multiple: true,
    options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student'],
    visibleBy: 'admin'
  })

  // checkbox: true/false value
  mockTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false,
    visibleBy: 'admin'
  })

  // date: a date string as the value
  mockTest({
    type: 'date',
    label: 'date succeeded',
    order: 5,
    required: false,
    visibleBy: 'members'
  })

  mockTest({
    type: 'file',
    fileTypes: ['document'],
    label: 'Registration form',
    description: 'Upload your registration form',
    order: 6,
    required: false,
    visibleBy: 'members',
    multiple: false
  })

  mockTest({
    type: 'file',
    fileTypes: ['photo'],
    label: 'Registration form',
    description: 'Upload your registration forms',
    order: 7,
    required: true,
    visibleBy: 'admin',
    multiple: true
  })

  mockTest({
    type: 'file',
    fileTypes: ['video'],
    label: 'Audition Video',
    description: 'Upload your audition video',
    order: 7,
    required: true,
    visibleBy: 'admin',
    multiple: false
  })

  mockTest({
    type: 'file',
    fileTypes: ['audio'],
    label: 'Audition Audio',
    description: 'Upload your audition audio',
    order: 7,
    required: true,
    visibleBy: 'admin',
    multiple: false
  })

  // multiple file types
  mockTest({
    type: 'file',
    fileTypes: ['audio', 'document'],
    label: 'License',
    description: 'Upload the front and back of your license',
    order: 7,
    required: true,
    visibleBy: 'admin',
    multiple: true
  })

  // failing test for empty customFields
  mockFailingTest(null)
  mockFailingTest({})

  // failing test for missing required field
  mockFailingTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false
  })

  // failing test for additional field
  mockFailingTest({
    type: 'checkbox',
    label: 'Living in NZ',
    order: 4,
    required: false,
    um: 'Not supposed to be here'
  })

  // failing test for missing options on list
  mockFailingTest({
    type: 'list',
    label: 'Employement Status',
    multiple: true,
    required: false,
    order: 3.3,
    visibleBy: 'admin'
  })

  // failing test for missing multiple
  mockFailingTest({
    type: 'list',
    label: 'Employement Status',
    required: false,
    order: 3.3,
    visibleBy: 'admin',
    options: ['Employed', 'Unemployed', 'Self-emplyed', 'Student']
  })

  // failing test for file types
  mockFailingTest({
    type: 'file',
    fileTypes: ['something'],
    label: 'Audition Audio',
    description: 'Upload your audition audio',
    order: 7,
    required: true,
    visibleBy: 'admin',
    multiple: false
  })

  // failing test for file missing multiple
  mockFailingTest({
    type: 'file',
    fileTypes: ['photo'],
    label: 'Audition Audio',
    description: 'Upload your audition audio',
    order: 7,
    required: true,
    visibleBy: 'admin'
  })

  // failing test for file missing description
  mockFailingTest({
    type: 'file',
    fileTypes: ['photo'],
    label: 'Audition Audio',
    order: 7,
    required: true,
    visibleBy: 'admin'
  })

  server.close()
})
