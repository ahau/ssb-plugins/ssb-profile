const { isMsgId } = require('ssb-ref')
const test = require('tape')
// const pull = require('pull-stream')
const { promisify: p } = require('util')
const Server = require('../../test-bot')

test('link.create/findAdminProfileLinks (link/profile-profile/admin)', async t => {
  // ////
  // Setup
  // ////
  const ssb = Server({ tribes: true })

  // create the group and admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: adminGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  // create an unowned profile in the parent group
  let details = {
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }
  const groupProfileId = await p(ssb.profile.person.group.create)(details)
  t.true(isMsgId(groupProfileId), 'person group profile was made')

  // create an unowned profile in the admin group
  details = {
    authors: {
      add: ['*']
    },
    recps: [adminGroupId]
  }
  const adminProfileId = await p(ssb.profile.person.admin.create)(details)
  t.true(isMsgId(adminProfileId), 'person admin profile was made')
  // TODO: which profile is the parent/child - determines recps

  // create the link between the two profiles
  const link = await p(ssb.profile.link.create)(groupProfileId, { profileId: adminProfileId })

  const expected = {
    key: link.key,
    type: 'link/profile-profile/admin',
    originalAuthor: ssb.id,
    parent: adminProfileId,
    child: groupProfileId,
    recps: [groupId],
    tombstone: null,
    conflictFields: [],
    states: []
  }

  t.deepEqual(
    link,
    {
      ...expected,
      /* legacy */
      value: {
        content: expected
      }
    },
    'link was made'
  )

  // find the links
  const groupProfileLinkedProfiles = await p(ssb.profile.person.group.findAdminProfileLinks)(groupProfileId)

  t.deepEqual(
    groupProfileLinkedProfiles,
    {
      childLinks: [],
      parentLinks: [expected]
    },
    'returns the admin profile as a parent link for the group profile'
  )

  const adminProfileLinkedProfiles = await p(ssb.profile.person.group.findAdminProfileLinks)(adminProfileId)

  t.deepEqual(
    adminProfileLinkedProfiles,
    {
      childLinks: [expected],
      parentLinks: []
    },
    'returns the group profile as a child link for the admin profile'
  )

  ssb.close()
  t.end()
})
