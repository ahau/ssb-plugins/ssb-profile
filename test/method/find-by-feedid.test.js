/* eslint-disable brace-style */
const test = require('tape')
const pull = require('pull-stream')
const Server = require('../test-bot')
const { replicate } = require('scuttle-testbot')

function setup (profileDetails, next) {
  const server = Server({ tribes: true })
  // server.post(m => console.log(m.value.content))
  let groupId
  if (profileDetails.some(detail => detail.recps)) {
    server.tribes.create({}, (err, info) => {
      if (err) throw err
      groupId = info.groupId
      publish()
    })
  }
  else publish()

  function publish () {
    pull(
      pull.values(profileDetails),
      pull.asyncMap(createProfileAndLink),
      pull.collect((err, links) => {
        if (err) throw err
        next({ server, links, groupId })
      })
    )
  }

  function createProfileAndLink ({ preferredName, avatarImage, gender, recps }, cb) {
    // NOTE gender default value is set to ensure tests pass as undefined is not a valid gender
    // be very careful in general publishing defaults, because you might assert something untrue

    const details = {
      preferredName,
      avatarImage,
      gender,
      authors: { add: [server.id] }
    }

    if (!avatarImage) delete details.avatarImage
    if (!gender) delete details.gender
    if (recps) details.recps = [groupId]

    const subType = recps ? 'group' : 'public'

    server.profile.person[subType].create(details, (err, profileId) => {
      if (err) return cb(err)
      server.profile.link.create(profileId, (err, link) => {
        if (err) return cb(err)
        server.get({ id: link.key, private: true, meta: true }, cb)
      })
    })
  }
}

test('findByFeedId finds all profiles', { objectPrintDepth: 15 }, t => {
  t.plan(6)

  const avatarImage = {
    blob: '&1ZQM7TjQHBUEcdBijB6y7dkX047wCf4aXcjFplTjrJo=.sha256',
    unbox: 'YmNz1XfPw/xkjoN594ZfE/JUhpYiGyOOQwNDf6DN+54=.boxs',
    mimeType: 'image/png',
    size: 512
  }

  const profileDetails = [
    { preferredName: 'John' },
    { preferredName: 'Mix', avatarImage },
    { preferredName: 'Mysterious Mix', gender: 'male', recps: true }
  ]

  console.log('working...')
  setup(profileDetails, ({ server, links, groupId }) => {
    server.profile.findByFeedId(server.id, (err, profiles) => {
      if (err) throw err

      const profileIds = links
        .map(link => link.value.content.child)

      t.equal(profiles.public.length, 2)
      t.deepEqual(profiles.public.map(p => p.key), profileIds.slice(0, 2), 'returns keys of users public profiles')
      t.equal(profiles.private.length, 1)
      t.deepEqual(profiles.private.map(p => p.key), profileIds.slice(2), 'returns keys of users private profiles')

      // NOTE oldest first!
      const publicState = [
        fullState({
          key: profiles.public[0].key,
          originalAuthor: server.id,
          type: 'person',
          preferredName: 'John',
          authors: {
            [server.id]: [{ start: 2, end: null }]
          },
          recps: null
        }),

        fullState({
          key: profiles.public[1].key,
          originalAuthor: server.id,
          type: 'person',
          preferredName: 'Mix',
          avatarImage,
          authors: {
            [server.id]: [{ start: 4, end: null }]
          },
          recps: null
        })
      ]

      const privateState = [
        fullState({
          key: profiles.private[0].key,
          originalAuthor: server.id,
          type: 'person',
          preferredName: 'Mysterious Mix',
          gender: 'male',
          authors: {
            [server.id]: [{ start: 6, end: null }]
          },
          recps: [groupId]
        }, { private: true })
      ]

      // can be used to "order" keys in an object for easier diffing with tap-diff
      // function order (obj) {
      //   return Object.entries(obj)
      //     .sort((a, b) => b[0] > a[0] ? -1 : 1)
      // }

      t.deepEqual(profiles.public, publicState, 'returns state of public profiles')
      t.deepEqual(profiles.private, privateState, 'returns state of private profiles')

      server.close()
    })
  })
})

test('findByFeedId finds no profile', t => {
  t.plan(1)

  setup([], ({ server, links }) => {
    server.profile.findByFeedId(server.id, (_, profiles) => {
      t.deepEqual(profiles.public, [], 'returns empty array of public profiles')

      server.close()
    })
  })
})

test('findByFeedId finds no tombstoned profiles', t => {
  t.plan(1)

  setup([{ preferredName: 'Max' }], ({ server, links }) => {
    const profileId = links[0].value.content.child
    const update = {
      tombstone: { date: Date.now(), reason: 'typo' }
    }
    server.profile.publicPerson.update(profileId, update, (err) => {
      if (err) throw err
      server.profile.publicPerson.get(profileId, (err, profile) => {
        if (err) throw err
        server.profile.findByFeedId(server.id, (err, profiles) => {
          if (err) throw err
          t.deepEqual(profiles.public, [], 'does not find tombstoned records!')

          server.close()
        })
      })
    })
  })
})

test('findByFeedId { selfLinkOnly: false }', t => {
  const alice = Server({ tribes: true })
  const kaitiaki = Server({ tribes: true })

  function setup (cb) {
    kaitiaki.tribes.create({}, (err, { groupId } = {}) => {
      t.error(err, 'group started')

      const profileA = {
        preferredName: 'alice (kaitiaki made)',
        authors: { add: [alice.id] },
        recps: [groupId]
      }
      kaitiaki.profile.privatePerson.create(profileA, (err, profileIdA) => {
        t.error(err, 'kaitiaki makes a profile for alice')

        kaitiaki.profile.link.create(profileIdA, { feedId: alice.id }, (err) => {
          t.error(err, 'kaitiaki links it to alices feedId')

          kaitiaki.tribes.invite(groupId, [alice.id], {}, (err) => {
            t.error(err, 'kaitiaki adds alice to group')

            replicate({ from: kaitiaki, to: alice }, (err) => {
              t.error(err, 'alice replicates from kaitiaki')

              // have to wait till rebuild is done... waiting 1000ms is not ideal
              setTimeout(
                () => {
                  const profileB = {
                    preferredName: 'alice',
                    authors: { add: [alice.id] },
                    recps: [groupId]
                  }
                  alice.profile.privatePerson.create(profileB, (err, profileIdB) => {
                    t.error(err, 'alice makes themself a profile in group')

                    alice.profile.link.create(profileIdB, (err) => {
                      t.error(err, 'alice links that profile to themself')

                      cb(null, { profileIdA, profileIdB })
                    })
                  })
                },
                1000
              )
            })
          })
        })
      })
    })
  }

  setup((_, { profileIdA, profileIdB }) => {
    alice.profile.findByFeedId(alice.id, { selfLinkOnly: false }, (err, results) => {
      if (err) throw err
      // {
      //   self: { public: [], private: [profileB] },
      //   other: { public: [], private: [profileA] }
      // }
      t.equal(results.self.private[0].key, profileIdB, 'findByFeedId finds profile alice made')
      t.equal(results.other.private[0].key, profileIdA, 'findByFeedId finds profile kaitiaki made')

      alice.close()
      kaitiaki.close()
      t.end()
    })
  })
})

function fullState (someState, opts = {}) {
  const common = {
    type: '???',
    authors: {},
    tombstone: null,

    conflictFields: [],
    states: []
  }

  const specific = opts.private === true
    ? {
        preferredName: null,
        legalName: null,
        altNames: [],

        description: null,

        avatarImage: null,
        headerImage: null,

        gender: null,
        source: null,
        aliveInterval: null,
        birthOrder: null,
        deceased: null,
        placeOfBirth: null,
        placeOfDeath: null,
        buriedLocation: null,

        city: null,
        country: null,
        postCode: null,

        profession: null,
        education: [],
        school: [],
        customFields: {}
      }
    : {
        preferredName: null,
        avatarImage: null,
        gender: null
      }

  return Object.assign(common, specific, someState)
}
